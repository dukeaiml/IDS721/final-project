use rust_bert::pipelines::sentiment::SentimentModel;
use serde::{Deserialize, Serialize};

pub struct SharedData {
    pub model: SentimentModel,
}

#[derive(Serialize)]
pub struct Sentiment {
    pub sentiment: String,
}

#[derive(Deserialize)]
pub struct Text {
    pub text: String,
}
