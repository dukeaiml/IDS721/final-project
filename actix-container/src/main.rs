use actix_web::{
    web::{self},
    App, HttpServer,
};
use rust_bert::pipelines::common::ModelResource;
use rust_bert::pipelines::common::ModelType::FNet;
use rust_bert::pipelines::sentiment::{SentimentConfig, SentimentModel};
use rust_bert::resources::LocalResource;
use std::path::PathBuf;
use std::sync::Mutex;

mod handlers;
mod models;
use handlers::put_file;
use models::SharedData;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();

    let config_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/transformer_files/config.json"),
    });

    let vocab_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/transformer_files/spiece.model"),
    });

    let model_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/transformer_files/rust_model.ot"),
    });

    let model_config: SentimentConfig = SentimentConfig {
        model_type: FNet,
        model_resource: ModelResource::Torch(model_resource),
        vocab_resource: vocab_resource,
        // merges_resource: Some(merges_resource),
        config_resource: config_resource,
        ..Default::default()
    };

    let model = SentimentModel::new(model_config).unwrap();

    let data: web::Data<Mutex<SharedData>> =
        web::Data::new(Mutex::new(SharedData { model: model }));

    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .route("/sentiment_analysis", web::post().to(put_file))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
