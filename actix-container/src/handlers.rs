use crate::models::{Sentiment, SharedData, Text};
use actix_web::{web, HttpResponse, Responder};
use std::format;
use std::sync::{Mutex, MutexGuard};

pub async fn put_file(data: web::Data<Mutex<SharedData>>, form: web::Json<Text>) -> impl Responder {
    // let vs: MutexGuard<'_, SharedData> = data.lock().unwrap();
    let vs: MutexGuard<'_, SharedData> = data.lock().unwrap();

    let text = form.text.clone();

    let input = [text.as_ref()];

    let prediction = vs.model.predict(&input);

    let response = Sentiment {
        sentiment: format!("{:?}", prediction),
    };

    HttpResponse::Ok().json(response)
}
