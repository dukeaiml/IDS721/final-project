# Builder stage
FROM rust:1.72 as builder
WORKDIR /usr/src/sentiment-service
COPY . .
RUN apt-get update && apt-get install -y python3-venv python3-pip && rm -rf /var/lib/apt/lists/*
RUN pip3 install torch==2.1.0+cpu --index-url https://download.pytorch.org/whl/cpu --break-system-packages
ENV LIBTORCH_USE_PYTORCH=1
RUN cargo build --release

# Final stage
FROM debian:latest

RUN apt-get update && apt-get install -y libfontconfig1 wget python3 python3-pip && \
    pip3 install torch==2.1.0+cpu --index-url https://download.pytorch.org/whl/cpu --break-system-packages && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/src/sentiment-service/target/release/sentiment-service /usr/local/bin/sentiment-service

# Download model files
RUN mkdir -p /usr/local/share/transformer_files && \
    wget -O /usr/local/share/transformer_files/config.json https://huggingface.co/gchhablani/fnet-base-finetuned-sst2/resolve/main/config.json?download=true && \
    wget -O /usr/local/share/transformer_files/spiece.model https://huggingface.co/google/fnet-base/resolve/main/spiece.model?download=true && \
    wget -O /usr/local/share/transformer_files/rust_model.ot https://huggingface.co/gchhablani/fnet-base-finetuned-sst2/resolve/main/rust_model.ot?download=true

ENV LD_LIBRARY_PATH=/usr/local/lib/python3.11/dist-packages/torch/lib:$LD_LIBRARY_PATH
CMD ["/usr/local/bin/sentiment-service"]
