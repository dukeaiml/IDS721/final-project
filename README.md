# Final Project

[![Pipeline Status](https://gitlab.com/dukeaiml/IDS721/final-project/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/final-project/-/pipelines)

This project consistes of a rust based container that serves a sentiment NLP model. Moreoever, this project contains the necessary config files to be deployed through AWS's managed kubernetes service(EKS) and the Gitlab CI/CD pipeline is also configured to deploy this project automatically. A demo video can be found at https://youtu.be/1R0cBY_WZk4.

[![Introduction to Project](https://img.youtube.com/vi/1R0cBY_WZk4/maxresdefault.jpg)](https://youtu.be/1R0cBY_WZk4)

## Tools and Technologies

- **Rust**: A systems programming language that guarantees memory safety and offers great performance.
- **Rust-Bert**: A rust library that provides end-to-end NLP pipelines.
- **Actix**: A rust library that provides high performance web service.
- **Docker**: A set of platform-as-a-service products that use OS-level virtualization to deliver software in packages called containers.
- **Hugging face**: A repository containing source of ML models.
- **AWS Elastic Container Registry (ECR)**: A fully managed Docker container registry that makes it easy for developers to store, manage, and deploy Docker container images.
- **AWS EKS**: A fully managed kubernetes service that makes it easy for developers to deploy their serverless functions in the AWS cloud.

## Running locally

Tp run this application directly in the local environment, you'll need to have Rust and Cargo installed.


1. **Clone the Repository**

    ```
    git clone https://your-repository-url.git
    cd your-project-directory
    ```

2.  **CD into the Source Code**

    ```
    cd actix-container
    ```

2. **Run directly**
    This command starts the application and exposes it on port 8080 of your localhost

    ```
    cargo run
    ```

## Running Containerized Locally

To run the containerized version of this application locally, you'll need to have Rust, Docker, and Cargo installed on your system.

1. **Clone the Repository**

    ```
    git clone https://your-repository-url.git
    cd your-project-directory
    ```

2.  **CD into the Source Code**

    ```
    cd actix-container
    ```

3. **Build the Docker Image**

    Navigate to the root of your project directory and run:

    ```
    docker build -t transformer .
    ```

4. **Run the Container**

    This command starts the container and exposes it on port 8080 of your localhost:

    ```
    docker run -p 8080:8080 transformer
    ```

## Deploying On AWS

This project can be deployed on AWS through AWS EKR and AWS EKS. AWS ECR is a container registry that is used to store built docker image in our case. AWS EKS is a convinient tool of deploying serveless functions. 

1. **Clone the Repository**

    You need to first make a fork of this project in your own gitlab environment.

2. **Set Up AWS ECR**

    You'll need to first create a Public AWS ECR Repo:

    ![Alt text](https://i.imgur.com/Fe1ixB5.png)


3. **Build the Docker Image and upload to AWS ECR**

    First, we need to log into AWS from our local Docker:
    ```
    aws ecr get-login-password --region <primary region> | docker login --username AWS --password-stdin <repo-id>.dkr.ecr.us-east-1.amazonaws.com
    ```
    Then, we need to build the target docker image:
    ```
    docker build -t transformer .
    ```
    Then, we need to tag the image:
    ```
    docker tag transformer:latest <repo-id>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest
    ```
    Lastly, we push the image to the ECR Repo:
    ```
    docker push <repo-id>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest
    ```

4. **Install the Necessary Tools**

    The easiest ways of deploying the Kubernetes cluster on AWS is through a command line tool [eksctl](https://eksctl.io/installation/). Moreoever, you'd also need to install [kubectl](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html) to interact with the cluster

5. **Deploy the Cluster on AWS EKS**

    First, we can use eksctl to deploy the cluster's infrastructure on EKS.
    ```
    eksctl create cluster --name <cluster name> --region <cluster region>
    ```
    Then, we can cd in to the directory containing the Kubernetes config files.
    ```
    cd EKS
    ```
    Lastly, we can use kubectl to deploy the application to the cluster.
    ```
    kubectl apply -f eks-sample-deployment.yaml
    kubectl apply -f eks-sample-service.yaml  
    ```

6. **Examine the Created Resources**

    Lastly, we can examine the created resources on AWS.
    We have a EKS cluster created:

    ![Alt tex](https://imgur.com/wNkWhFq.png)

    Additionally We have some EC2 instances deployed as compute resources:

    ![Alt text](https://i.imgur.com/ZDrRNDB.png)

    Moreoever, we have a load balancer deployed to direct application load:

    ![Alt text](https://i.imgur.com/2TBN6Pk.png)

## Using the Application

Once a version of this application is running, we can then test the functionality of the application.

### Run Kubernetes App

This application can be used to determine the sentiment of a body of text. We can send the following json body to $DNS_name_of_the_load_balancer/sentiment_analysis: 
```
{
    "text": "Probably my all-time favorite movie, a story of selflessness, sacrifice and dedication to a noble cause, but it's not preachy or boring."
}
```
This specifies the body of text we want it to summarize. The lambda will return the following result:
```
{
    "sentiment": "[Sentiment { polarity: Negative, score: 0.9983460903167725 }]"
}
```

![Alt text](https://i.imgur.com/sBpTgUU.png)

### Configure HTTP EndPoint 

We can also do some additional work to configure a HTTP endpoint for our Kubernetes App. To achieve this, we can register for a domain name under the AWS's Route53 service and register an A record pointing to the load balancer(Here I am registering the A record for service.ghflow.com):

![Alt text](https://i.imgur.com/NZevmyW.png)

Then, the application can be accessed through the simplified DNS name:

![Alt text](https://i.imgur.com/BJWopS9.png)

Additionally, we can use the AWS ACM certificate manager to create a certificate for our domain name. We can then open up port 443 on our load balancer's listener:

![Alt text](https://i.imgur.com/foR7LWB.png)

This way, we have essentially configured our endpoint such that it supports HTTPS:

![Alt text](https://i.imgur.com/BJWopS9.png)

### Logging and Monitoring

We can use a simple command with eksctl to enable logging for the Kubernetes application: `eksctl utils update-cluster-logging --enable-types all --cluster
 my-cluster --region us-east-1 --approve`. Then, we can view the logs generated by the application through CloudWatch:

![Alt text](https://i.imgur.com/rZgT2xs.png)






